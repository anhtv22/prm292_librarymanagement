package fu.spr8.prm292_librarymanagement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class BookDatabase {
    static List<BookModel> books = new ArrayList<>();
    static final String DATABASE_NAME = "LibraryDatabase.db";
    static final String TABLE_NAME = "Books";
    static final int DATABASE_VERSION = 1;
    static final String CREATE_DATABASE = "create table " + TABLE_NAME + "(ID integer primary key autoincrement,ISBN  text,Title  text,Author text,Category text,Year text,Edition text,Language text); ";
    private static final String TAG = "BookDatabase";
    public static SQLiteDatabase db;
    private Context context;
    private static DatabaseHelper dbHelper;

    public BookDatabase(Context context1) {
        context = context1;
        dbHelper = new DatabaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //open db
    public BookDatabase open() throws SQLException {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    //close db
    public void close() {
        db.close();
    }

    public SQLiteDatabase getDatabaseInstance() {
        return db;
    }

    public static List<BookModel> getAllBooks() {
        books.clear();
        db = dbHelper.getReadableDatabase();
        String query = "SELECT ID, ISBN, Title, Author, Category, Year, Edition, Language FROM Books";
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String id = cursor.getString(0);
            String isbn = cursor.getString(1);
            String title = cursor.getString(2);
            String author = cursor.getString(3);
            String category = cursor.getString(4);
            String year = cursor.getString(5);
            String edition = cursor.getString(6);
            String language = cursor.getString(7);

            books.add(new BookModel(id, isbn, title, author, category, year, edition, language));
            cursor.moveToNext();
        }

        cursor.close();

        return books;
    }

    public static BookModel getBookById(int ID) {
        BookModel book = null;
        db = dbHelper.getReadableDatabase();
        String query = "SELECT ID, ISBN, Title, Author, Category, Year, Edition, Language FROM Books WHERE ID = ?";
        Cursor cursor = db.rawQuery(query, new String[]{ID + ""});

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            String id = cursor.getString(0);
            String isbn = cursor.getString(1);
            String title = cursor.getString(2);
            String author = cursor.getString(3);
            String category = cursor.getString(4);
            String year = cursor.getString(5);
            String edition = cursor.getString(6);
            String language = cursor.getString(7);

            book = new BookModel(id, isbn, title, author, category, year, edition, language);
        }
        cursor.close();
        return book;
    }

    public String insertBook(BookModel book) {
        try {
            ContentValues newValues = new ContentValues();

            newValues.put("ISBN", book.getISBN());
            newValues.put("Title", book.getTitle());
            newValues.put("Author", book.getAuthor());
            newValues.put("Category", book.getCategory());
            newValues.put("Year", book.getYear());
            newValues.put("Edition", book.getEdition());
            newValues.put("Language", book.getLanguage());

            db = dbHelper.getWritableDatabase();
            long result = db.insert(TABLE_NAME, null, newValues);
            Log.i("Row Insert Result ", String.valueOf(result));
            Utils.showToast(this.context.getApplicationContext(), "Add Book Successfully!");
            db.close();

        } catch (Exception ex) {
        }
        return "ok";
    }

    public void updateBook(BookModel book) {
        ContentValues updatedValues = new ContentValues();

        updatedValues.put("Title", book.getTitle());
        updatedValues.put("Author", book.getAuthor());
        updatedValues.put("Category", book.getCategory());
        updatedValues.put("Year", book.getYear());
        updatedValues.put("Edition", book.getEdition());
        updatedValues.put("Language", book.getLanguage());

        String where = "ID = ?";
        db = dbHelper.getReadableDatabase();
        db.update(TABLE_NAME, updatedValues, where, new String[]{book.getID()});

        db.close();

        Utils.showToast(context.getApplicationContext(), "Book Updated!");
    }

    public int deleteBookByID(String ID) {
        String where = "ID=?";
        int rowDeleted = db.delete(TABLE_NAME, where, new String[]{ID});
        Utils.showToast(this.context.getApplicationContext(), "Deleted Successfully!");
        return rowDeleted;
    }

    public int getBookQuantity() {
        db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        db.close();
        return cursor.getCount();
    }

    public void deleteAllBook() {
        db = dbHelper.getReadableDatabase();
        db.delete(TABLE_NAME, "1", null);
        db.close();
        Utils.showToast(context.getApplicationContext(), "List Book Empty!");
    }
}
