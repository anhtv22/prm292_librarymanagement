package fu.spr8.prm292_librarymanagement;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    BookDatabase bookDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bookDatabase = new BookDatabase(getApplicationContext());
    }

    public void insertBookActivity(View view) {
        Intent myIntent = new Intent(MainActivity.this, InsertBookActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    public void updateBookView(View view) {
        Intent myIntent = new Intent(MainActivity.this, UpdateBookActivity.class);
        MainActivity.this.startActivity(myIntent);
    }


    public void deleteBookActivity(View view) {
        Intent myIntent = new Intent(MainActivity.this, DeleteBookActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    public void deleteAllBook(View view) {
        bookDatabase.deleteAllBook();
    }
}