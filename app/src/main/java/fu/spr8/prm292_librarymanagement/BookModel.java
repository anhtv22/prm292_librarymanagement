package fu.spr8.prm292_librarymanagement;

public class BookModel {
    private String ID;
    private String ISBN;
    private String Title;
    private String Author;
    private String Category;
    private String Year;
    private String Edition;
    private String Language;

    public BookModel() {
    }

    public BookModel(String ID, String ISBN, String title, String author, String category, String year, String edition, String language) {
        this.ID = ID;
        this.ISBN = ISBN;
        Title = title;
        Author = author;
        Category = category;
        Year = year;
        Edition = edition;
        Language = language;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getEdition() {
        return Edition;
    }

    public void setEdition(String edition) {
        Edition = edition;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    @Override
    public String toString() {
        return "BookModel{" +
                "ID='" + ID + '\'' +
                ", ISBN='" + ISBN + '\'' +
                ", Title='" + Title + '\'' +
                ", Author='" + Author + '\'' +
                ", Category='" + Category + '\'' +
                ", Year='" + Year + '\'' +
                ", Edition='" + Edition + '\'' +
                ", Language='" + Language + '\'' +
                '}';
    }
}
